#!/bin/bash

set -e

docker run --rm -it -v "$PWD:$PWD" -w "$PWD" quay.io/pypa/manylinux_2_28_x86_64 bash ./ci/build.sh
